package com.citi.training.assignment.hackathon;

import com.citi.training.assignment.hackathon.model.Library;
import com.citi.training.assignment.hackathon.service.LibraryService;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class LibraryServiceTest {

    @Autowired
    private LibraryService libraryService;
    
    @Test
    public void test_save_sanityCheck() {
        Library testRecord = new Library();
        testRecord.setName("1984");
        testRecord.setType("Book");
        testRecord.setBorrowed(true);

        libraryService.save(testRecord);
    }

    @Test
    public void test_findAll_sanityCheck() {
        assert(libraryService.findAll().size() > 0);
    }
    
}
