package com.citi.training.assignment.hackathon.service;

import java.util.List;

import com.citi.training.assignment.hackathon.dao.LibraryMongoRepo;
import com.citi.training.assignment.hackathon.model.Library;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component 
public class LibraryService {

    @Autowired
    private LibraryMongoRepo mongoRepo;

    public List<Library> findAll(){
        return mongoRepo.findAll();
    }
    
    public Library save(Library library){
        return mongoRepo.save(library);
    }
    
}
