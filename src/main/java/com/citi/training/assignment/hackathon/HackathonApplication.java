package com.citi.training.assignment.hackathon;

import java.util.Scanner;

import com.citi.training.assignment.hackathon.model.Library;
import com.citi.training.assignment.hackathon.service.LibraryService;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HackathonApplication {

	public static void main(String[] args) {

		SpringApplication.run(HackathonApplication.class, args);
		Scanner sc = new Scanner(System.in);
		LibraryService ls = new LibraryService();

		do {
			System.out.println("What would you like to to do? \na for add\nb for borrow\nr for remove\nx for exit");
			if (sc.nextLine().equalsIgnoreCase("a")) {
				System.out.println("What do you want to add?\n\n1. Book\n2. CD\n3. DVD\n4. Periodical");
				if (sc.next().equals("1")) {
					Library library = new Library();

					library.setType("Book");
					System.out.println("What is the name of the book?");
					library.setName(sc.next());
					library.setBorrowed(false);

					ls.save(library);
					System.out.println(library.getName()+" added to library.");
				}
			}
		} while(!sc.next().equalsIgnoreCase("x"));
		sc.close();

	}
}
