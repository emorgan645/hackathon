package com.citi.training.assignment.hackathon.model;

public class Library {

    private String id;
    private Boolean borrowed;
    private String type;
    private String name;

    public Library() {
    }

    public Library(Boolean borrowed, String type, String name) {
        this.borrowed = borrowed;
        this.type = type;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Boolean getBorrowed() {
        return borrowed;
    }

    public void setBorrowed(Boolean borrowed) {
        this.borrowed = borrowed;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    
}
