package com.citi.training.assignment.hackathon.dao;

import com.citi.training.assignment.hackathon.model.Library;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Component;

@Component
public interface LibraryMongoRepo extends MongoRepository<Library, String> {
    
}
    
